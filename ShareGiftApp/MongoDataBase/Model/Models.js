"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const path = require("path");
const log_1 = __importDefault(require("../../MyLog/log"));
const UserSchame = new mongoose_1.default.Schema({
    UserName: String,
    UserId: {
        type: String,
        unique: true
    },
    GiftCode: String,
    KeysFree: Number,
    KeysUse: Number,
    LoginType: Number,
    NameUserShareLink: String,
    NameLoginType: String,
    CreatedAt: Date
});
const UserDocument = mongoose_1.default.model('User', UserSchame);
const GiftCodeFreeSchame = new mongoose_1.default.Schema({
    Value: {
        type: String,
        unique: true
    },
    CreatedAt: Date
});
const GiftCodeFreeDocument = mongoose_1.default.model('GiftCodeFree', GiftCodeFreeSchame);
const GiftCodeUsedSchame = new mongoose_1.default.Schema({
    Value: {
        type: String,
        unique: true
    },
    User_Id: String,
    UserId: String,
    UserName: String,
    CreatedAt: Date
});
const GiftCodeUsedDocument = mongoose_1.default.model('GiftCodeUsed', GiftCodeUsedSchame);
const ShareLinkSchame = new mongoose_1.default.Schema({
    UserShare_Id: String,
    UserShareId: String,
    UsersClickLink: [
        {
            _Id: String,
            UserId: String,
            UserName: String
        }
    ],
    CreatedAt: Date
});
const ShareLinkDocument = mongoose_1.default.model('ShareLink', ShareLinkSchame);
class MongodbModel {
    static FindUserByUserId(userId, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield UserDocument.findOne({ UserId: userId }, (err, res) => {
                    if (err) {
                    }
                    else {
                        if (callback != null) {
                            callback(res);
                        }
                    }
                }).exec();
            }
            catch (err) {
                let fileName = path.basename(__filename);
                let Tag = fileName + "_" + "FindUserByUserId " + userId;
                log_1.default.error(Tag, err);
            }
        });
    }
    static UpdateGiftCodeUserById(id, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield UserDocument.updateOne({ _id: id }, (err, res) => {
                    if (err) {
                    }
                    else {
                        if (callback != null) {
                            callback(res);
                        }
                    }
                }).exec();
            }
            catch (err) {
                let fileName = path.basename(__filename);
                let Tag = fileName + "_" + "UpdateGiftCodeUserById " + id;
                log_1.default.error(Tag, err);
            }
        });
    }
    static FindShareLinkById(id, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield ShareLinkDocument.findOne({ _id: id }, (err, res) => {
                    if (err) {
                    }
                    else {
                        if (callback != null) {
                            callback(res);
                        }
                    }
                }).exec();
            }
            catch (err) {
                let fileName = path.basename(__filename);
                let Tag = fileName + "_" + "FindShareLinkById " + id;
                log_1.default.error(Tag, err);
            }
        });
    }
    static GetAndDeleteGiftCodeFree(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield GiftCodeFreeDocument.findOneAndDelete({}, {}).sort({ _id: -1 }).exec().then((res) => {
                    if (callback != null) {
                        callback(res);
                    }
                });
            }
            catch (err) {
                let fileName = path.basename(__filename);
                let Tag = fileName + "_" + "GetAndDeleteGiftCodeFree ";
                log_1.default.error(Tag, err);
            }
        });
    }
    static User(_UserName, _UserId, _GiftCode, _KeysFree, _KeysUse, _LoginType, _NameLoginType, _CreatedAt) {
        var UserDoc = new UserDocument({
            UserName: _UserName,
            UserId: _UserId,
            GiftCode: _GiftCode,
            KeysFree: _KeysFree,
            KeysUse: _KeysUse,
            LoginType: _LoginType,
            NameUserShareLink: "",
            NameLoginType: _NameLoginType,
            CreatedAt: _CreatedAt
        });
        return UserDoc;
    }
    static GiftCodeFree(_Value, _CreatedAt) {
        var GiftCodeDoc = new GiftCodeFreeDocument({
            Value: _Value,
            CreatedAt: _CreatedAt
        });
        return GiftCodeDoc;
    }
    static GiftCodeUsed(_Value, _User_Id, _UserId, _UserName, _CreatedAt) {
        var GiftCodeDoc = new GiftCodeUsedDocument({
            Value: _Value,
            User_Id: _User_Id,
            UserId: _UserId,
            UserName: _UserName,
            CreatedAt: _CreatedAt
        });
        return GiftCodeDoc;
    }
    static ShareLink(_UserShare_Id, _UserShareId, _UsersClickLink, _CreatedAt) {
        var ShareLinkDoc = new ShareLinkDocument({
            UserShare_Id: _UserShare_Id,
            UserShareId: _UserShareId,
            UsersClickLink: _UsersClickLink,
            CreatedAt: _CreatedAt
        });
        return ShareLinkDoc;
    }
}
exports.MongodbModel = MongodbModel;
//# sourceMappingURL=Models.js.map