import mongoose, { Document } from 'mongoose';
import path = require('path');
import logger from '../../MyLog/log';
export interface UserInfo extends Document {
    UserName: string;
    UserId: string;
    GiftCode: string;
    KeysFree: number;
    KeysUse: number;
    LoginType: number;
    NameUserShareLink: string;
    NameLoginType: string;
    CreatedAt: Date;
}
const UserSchame = new mongoose.Schema(
    {
        UserName: String,
        UserId:
        {
            type: String,
            unique: true
        },
        GiftCode: String,
        KeysFree: Number,
        KeysUse: Number,
        LoginType: Number,
        NameUserShareLink: String,
        NameLoginType: String,
        CreatedAt: Date
    }
);
const UserDocument = mongoose.model<UserInfo>('User', UserSchame);

export interface GiftCodeFree extends Document {
    Value: string;
    CreatedAt: Date;
}
const GiftCodeFreeSchame = new mongoose.Schema(
    {
        Value: {
            type: String,
            unique: true
        },
        CreatedAt: Date
    }
);
const GiftCodeFreeDocument = mongoose.model<GiftCodeFree>('GiftCodeFree', GiftCodeFreeSchame);

export interface GiftCodeUsed extends Document{
    Value: string;
    User_Id: string;
    UserId: string;
    UserName: string;
    CreatedAt: Date;
}
const GiftCodeUsedSchame = new mongoose.Schema(
    {
        Value: {
            type: String,
            unique: true
        },
        User_Id: String,
        UserId: String,
        UserName: String,
        CreatedAt: Date
    }
);
const GiftCodeUsedDocument = mongoose.model<GiftCodeUsed>('GiftCodeUsed', GiftCodeUsedSchame);

export interface ShareLink extends Document{
    UserShare_Id: string;
    UserShareId: string;
    UsersClickLink: [
        {
            _Id: string,
            UserId: string,
            UserName: string
        }
    ];
    CreatedAt: Date;
}
const ShareLinkSchame = new mongoose.Schema(
    {
        UserShare_Id: String,
        UserShareId: String,
        UsersClickLink: [
            {
                _Id: String,
                UserId: String,
                UserName: String
            }
        ],
        CreatedAt: Date
    }
);
const ShareLinkDocument = mongoose.model<ShareLink>('ShareLink', ShareLinkSchame);
class MongodbModel
{
    public static async FindUserByUserId(userId: string, callback: (doc: UserInfo) => void){
        try {
            await UserDocument.findOne({ UserId: userId }, (err, res) => {
                if (err) {
                } else {
                    if (callback != null) {
                        callback(res);
                    }
                }
            }).exec();
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "FindUserByUserId " + userId;
            logger.error(Tag, err);
        }
    }
    public static async UpdateGiftCodeUserById(id: string, callback: (doc: GiftCodeUsed) => void) {
        try {
            await UserDocument.updateOne({ _id: id }, (err, res) => {
                if (err) {
                } else {
                    if (callback != null) {
                        callback(res);
                    }
                }
            }).exec();
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "UpdateGiftCodeUserById " + id;
            logger.error(Tag, err);
        }
        
    }
    public static async FindShareLinkById(id: string, callback: (doc: ShareLink) => void) {
        try {
            await ShareLinkDocument.findOne({ _id: id }, (err, res) => {
                if (err) {
                } else {
                    if (callback != null) {
                        callback(res);
                    }
                }
            }).exec();
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "FindShareLinkById " + id;
            logger.error(Tag, err);
        }
        
    }
    public static async GetAndDeleteGiftCodeFree(callback: (doc: GiftCodeFree) => void){
        try {
            await GiftCodeFreeDocument.findOneAndDelete({}, {}).sort({ _id: -1 }).exec().then((res) => {
                if (callback != null) {
                    callback(res);
                }
            });
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "GetAndDeleteGiftCodeFree ";
            logger.error(Tag, err);
        }
    }

    public static User(_UserName: string, _UserId: string, _GiftCode: string, _KeysFree: number, _KeysUse: number, _LoginType: number, _NameLoginType: string, _CreatedAt: Date): UserInfo {
        var UserDoc = new UserDocument({
            UserName: _UserName,
            UserId: _UserId,
            GiftCode: _GiftCode,
            KeysFree: _KeysFree,
            KeysUse: _KeysUse,
            LoginType: _LoginType,
            NameUserShareLink:"",
            NameLoginType: _NameLoginType,
            CreatedAt: _CreatedAt
        });
        return UserDoc;
    }
    

    public static GiftCodeFree(_Value: string, _CreatedAt: Date): GiftCodeFree {
        var GiftCodeDoc = new GiftCodeFreeDocument({
            Value: _Value,
            CreatedAt: _CreatedAt
        });
        return GiftCodeDoc;
    }
    public static GiftCodeUsed(_Value: string, _User_Id: string, _UserId: string, _UserName: string, _CreatedAt: Date): GiftCodeUsed {
        var GiftCodeDoc = new GiftCodeUsedDocument({
            Value: _Value,
            User_Id: _User_Id,
            UserId: _UserId,
            UserName: _UserName,
            CreatedAt: _CreatedAt
        });
        return GiftCodeDoc;
    }
    public static ShareLink(_UserShare_Id: string, _UserShareId: string, _UsersClickLink: Array<Object>, _CreatedAt: Date): ShareLink {
        var ShareLinkDoc = new ShareLinkDocument({
            UserShare_Id: _UserShare_Id,
            UserShareId: _UserShareId,
            UsersClickLink: _UsersClickLink,
            CreatedAt: _CreatedAt
        });
        return ShareLinkDoc;
    }
}
export { MongodbModel}