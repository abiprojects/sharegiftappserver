"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const log_1 = __importDefault(require("../MyLog/log"));
const path = require("path");
class MongodbControl {
    static connect(url) {
        try {
            mongoose_1.default.connect(url, { useUnifiedTopology: true, useNewUrlParser: true });
            var db = mongoose_1.default.connection;
            db.on('error', console.error.bind(console, 'connection error:'));
            db.once('open', function () {
                log_1.default.info("MongodbControl", "connect mongo success");
            });
        }
        catch (err) {
            let fileName = path.basename(__filename);
            let Tag = fileName + "_" + "connect mongo";
            log_1.default.error(Tag, err);
        }
    }
    static Insert(doc, callback) {
        try {
            doc.save((err, data) => {
                if (callback != null) {
                    if (err)
                        callback(false);
                    else
                        callback(true);
                }
            });
        }
        catch (err) {
            let fileName = path.basename(__filename);
            let Tag = fileName + "_" + "Insert " + doc.toString();
            log_1.default.error(Tag, err);
        }
    }
}
exports.MongodbControl = MongodbControl;
//# sourceMappingURL=MongodbController.js.map