import mongoose from 'mongoose';
import logger from '../MyLog/log';
import path = require('path');

class MongodbControl {
    public static connect(url: string) {
        try {
            mongoose.connect(url, { useUnifiedTopology: true, useNewUrlParser: true});
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error:'));
            db.once('open', function () {
                logger.info("MongodbControl", "connect mongo success");
            });
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "connect mongo";
            logger.error(Tag, err);
        }
        
    }
    public static Insert<T extends mongoose.Document>(doc: T, callback: (status: boolean) => void) {
        try {
            doc.save((err, data: T) => {
                if (callback != null) {
                    if (err) callback(false);
                    else callback(true);
                }
            });
        } catch (err) {
            let fileName: string = path.basename(__filename);
            let Tag: string = fileName + "_" + "Insert " + doc.toString();
            logger.error(Tag, err);
        }
        
        
    }
}
export { MongodbControl }
