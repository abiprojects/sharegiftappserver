"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log4js_1 = __importDefault(require("log4js"));
log4js_1.default.configure({
    appenders: {
        logError: { type: 'dateFile', filename: './log/Error/logError.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logInfo: { type: 'dateFile', filename: './log/Info/logInfo.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logDebug: { type: 'dateFile', filename: './log/Debug/logDebug.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logWarning: { type: 'dateFile', filename: './log/Warning/logWarning.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true }
    },
    categories: {
        default: { appenders: ['logInfo'], level: 'info' },
        logDebug: { appenders: ['logDebug'], level: 'debug' },
        logWarning: { appenders: ['logWarning'], level: 'warn' },
        logError: { appenders: ['logError'], level: 'error' }
    }
});
class logger {
    static info(TAG, message) {
        log4js_1.default.getLogger('default').info(TAG + ": " + message);
    }
    static error(TAG, message) {
        log4js_1.default.getLogger('logError').info(TAG + ": " + message);
    }
    static debug(TAG, message) {
        log4js_1.default.getLogger('logDebug').info(TAG + ": " + message);
    }
    static Warning(TAG, message) {
        log4js_1.default.getLogger('logWarning').info(TAG + ": " + message);
    }
}
exports.default = logger;
//# sourceMappingURL=log.js.map