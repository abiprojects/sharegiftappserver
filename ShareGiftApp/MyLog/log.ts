import log4js from 'log4js';
log4js.configure({
    appenders: {
        logError: { type: 'dateFile', filename: './log/Error/logError.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logInfo: { type: 'dateFile', filename: './log/Info/logInfo.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logDebug: { type: 'dateFile', filename: './log/Debug/logDebug.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true },
        logWarning: { type: 'dateFile', filename: './log/Warning/logWarning.log', pattern: "-dd-MM-yyyy.txt", maxLogSize: 20480, backups: 10, alwaysIncludePattern: true }
    },
    categories: {
        default: { appenders: ['logInfo'], level: 'info' },
        logDebug: { appenders: ['logDebug'], level: 'debug' },
        logWarning: { appenders: ['logWarning'], level: 'warn' },
        logError: { appenders: ['logError'], level: 'error' }
    }
});
export default class logger {
    public static info(TAG: string, message: string) {
        log4js.getLogger('default').info(TAG + ": " + message);
    }
    public static error(TAG: string, message: string) {
        log4js.getLogger('logError').info(TAG + ": " + message);
    }
    public static debug(TAG: string, message: string) {
        log4js.getLogger('logDebug').info(TAG + ": " + message);
    }
    public static Warning(TAG: string, message: string) {
        log4js.getLogger('logWarning').info(TAG + ": " + message);
    }
}