"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const debug = require("debug");
const express = require("express");
const path = require("path");
const cors_1 = __importDefault(require("cors"));
const bodyParser = require("body-parser");
const index_1 = __importDefault(require("./routes/index"));
const user_1 = __importDefault(require("./routes/user"));
const MongodbController_1 = require("./MongoDataBase/MongodbController");
const log_1 = __importDefault(require("./MyLog/log"));
//const urlMongodb = 'mongodb://127.0.0.1:27017/AbiGiftCode';
const urlMongodb = 'mongodb://mongo3.4:27017/AbiGiftCode';
MongodbController_1.MongodbControl.connect(urlMongodb);
var app = express();
app.use(cors_1.default());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
//app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'public/WebGL'), {
    setHeaders: function (res, path) {
        if (path.indexOf("unityweb") !== -1) {
            //res.setHeader('Content-Encoding', 'gzip');
        }
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', index_1.default);
app.use('/users', user_1.default);
//var i: number;
//for (i = 1; i <= 100; i++) {
//    let giftcode = MongodbModel.GiftCodeFree("giftcode"+i, new Date()); 
//    giftcode.save();
//}
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    //res.setHeader("Access-Control-Allow-Origin", "*");
    //res.setHeader("Access-Control-Allow-Methods", "PUT,GET,DELETE,PATCH");
    //res.setHeader('Access-Control-Allow-Credentials', 'true');
    //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Origin,Accept,Authorization');
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    //res.setHeader('Access-Control-Allow-Credentials', 'true');
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        //res.setHeader("Access-Control-Allow-Origin", "*");
        //res.setHeader("Access-Control-Allow-Methods", "PUT,GET,DELETE,PATCH");
        //res.setHeader('Access-Control-Allow-Credentials', 'true');
        //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Origin,Accept,Authorization');
        //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
        //res.setHeader('Access-Control-Allow-Credentials', 'true');
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
    //res.setHeader("Access-Control-Allow-Origin", "*");
    //res.setHeader("Access-Control-Allow-Methods", "PUT,GET,DELETE,PATCH");
    //res.setHeader('Access-Control-Allow-Credentials', 'true');
    //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Origin,Accept,Authorization');
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    //res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
    log_1.default.info("app.listen", 'Express server listening on port ' + server.address().port);
});
//# sourceMappingURL=app.js.map