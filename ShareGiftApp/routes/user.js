"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET users listing.
 */
const express = require("express");
const MongodbController_1 = require("../MongoDataBase/MongodbController");
const Models_1 = require("../MongoDataBase/Model/Models");
const log_1 = __importDefault(require("../MyLog/log"));
const path = require("path");
const fs = require("fs");
const router = express.Router();
const LinkGG = 'https://play.google.com/store/apps/details?id=com.alien.shooter.galaxy.attack';
const linkApple = 'https://apps.apple.com/US/app/id1176011642?mt=8';
const linkVideoLogin = 'https://sandbox.abigames.com.vn/nodejs/users/download';
const linkVideoEvent = 'https://sandbox.abigames.com.vn/nodejs/users/download';
const UrlItemGiftCode = 'http://codeskulptor-demos.commondatastorage.googleapis.com/GalaxyInvaders/alien_7_3.png';
const LinkShareEvent = 'https://www.facebook.com/GalaxyAttack/';
const LinkFanpageFB = 'https://www.facebook.com/GalaxyAttack/';
const Title1 = 'Chúc mừng bạn đã nhận đc gift code';
const Title2 = 'Event abcxyz sắp ra mắt ..............';
const TermsAndConditions = '1. Vui lòng giữ và lưu mã phần thưởng, chỉ có thể sử dụng nó 1 lần trước khi hết hạn.\
2. Mã phần thưởng sẽ hết hạn vào 1 ngày đẹp trời nào đó';
var LoginType;
(function (LoginType) {
    LoginType[LoginType["FB"] = 0] = "FB";
    LoginType[LoginType["Line"] = 1] = "Line";
    LoginType[LoginType["Kakao"] = 2] = "Kakao";
})(LoginType || (LoginType = {}));
function updateShareLink(sharelinkId, user) {
    if (sharelinkId != "") {
        Models_1.MongodbModel.FindShareLinkById(sharelinkId, (sharelink) => __awaiter(this, void 0, void 0, function* () {
            if (sharelink != null) {
                let UserShareId = sharelink.get("UserShareId", String);
                Models_1.MongodbModel.FindUserByUserId(UserShareId, (userShare) => {
                    if (userShare != null) {
                        let giftcode = userShare.get("GiftCode", String);
                        if (giftcode != "") {
                        }
                        else {
                            Models_1.MongodbModel.GetAndDeleteGiftCodeFree((giftCodeFree) => {
                                if (giftCodeFree != null) {
                                    try {
                                        let giftcodevalue = giftCodeFree.Value;
                                        let userShare_id = userShare._id;
                                        let userShareid = userShare.UserId;
                                        let userShareName = userShare.UserName;
                                        let giftCodeUsed = Models_1.MongodbModel.GiftCodeUsed(giftcodevalue, userShare_id, userShareid, userShareName, new Date());
                                        MongodbController_1.MongodbControl.Insert(giftCodeUsed, null);
                                        let _NameUserShareLink = user.UserName;
                                        log_1.default.info("", "_NameUserShareLink = " + _NameUserShareLink);
                                        if (userShare.GiftCode == "")
                                            userShare.GiftCode = giftcodevalue;
                                        if (userShare.KeysFree < 2)
                                            userShare.KeysFree += 1;
                                        if (userShare.NameUserShareLink == "")
                                            userShare.NameUserShareLink = _NameUserShareLink;
                                        userShare.save();
                                    }
                                    catch (err) {
                                        log_1.default.error("Update usershare ", err);
                                    }
                                }
                            });
                        }
                    }
                });
                let user_id = user._id;
                let userid = user.UserId;
                let userName = user.UserName;
                let UserClick = {
                    _Id: user_id,
                    UserId: userid,
                    UserName: userName
                };
                sharelink.UsersClickLink.push(UserClick);
                yield sharelink.save();
            }
        }));
    }
}
router.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let fileName = path.basename(__filename);
        let Tag = fileName + "_" + "post /";
        let name = req.body['name'];
        let id = req.body['id'];
        let sharelinkId = req.body['sharelinkId'];
        let login_type = req.body['login_type'];
        let type = LoginType[login_type];
        log_1.default.info(Tag, JSON.stringify(req.body));
        yield Models_1.MongodbModel.FindUserByUserId(id, (user) => {
            if (user != null) {
                if (user.GiftCode != "") {
                    let data = {
                        'status': true,
                        'LinkGG': LinkGG,
                        'linkApple': linkApple,
                        'linkVideoLogin': linkVideoLogin,
                        'linkVideoEvent': linkVideoEvent,
                        'UrlItemGiftCode': UrlItemGiftCode,
                        'LinkShareEvent': LinkShareEvent,
                        'LinkFanpageFB': LinkFanpageFB,
                        'Title1': Title1,
                        'Title2': Title2,
                        'TermsAndConditions': TermsAndConditions,
                        'data': user
                    };
                    res.send(data);
                    log_1.default.info(Tag + "user != null", JSON.stringify(data));
                }
                else {
                    let data = {
                        'status': true,
                        'LinkGG': LinkGG,
                        'linkApple': linkApple,
                        'linkVideoLogin': linkVideoLogin,
                        'linkVideoEvent': linkVideoEvent,
                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'LinkFanpageFB': LinkFanpageFB,
                        'Title1': '',
                        'Title2': '',
                        'TermsAndConditions': '',
                        'data': user
                    };
                    res.send(data);
                    log_1.default.info(Tag + "user != null", JSON.stringify(data));
                }
                updateShareLink(sharelinkId, user);
            }
            else {
                let user = Models_1.MongodbModel.User(name, id, '', 1, 0, login_type, LoginType[type], new Date());
                MongodbController_1.MongodbControl.Insert(user, (status) => __awaiter(void 0, void 0, void 0, function* () {
                    if (status) { //insert thanh cong
                        let data = {
                            'status': status,
                            'LinkGG': LinkGG,
                            'linkApple': linkApple,
                            'linkVideoLogin': linkVideoLogin,
                            'linkVideoEvent': linkVideoEvent,
                            'UrlItemGiftCode': '',
                            'LinkShareEvent': LinkShareEvent,
                            'LinkFanpageFB': LinkFanpageFB,
                            'Title1': '',
                            'Title2': '',
                            'TermsAndConditions': '',
                            'data': user
                        };
                        res.send(data);
                        log_1.default.info(Tag + "user == null && insert thanh cong", JSON.stringify(data));
                        if (sharelinkId != "") {
                            Models_1.MongodbModel.FindShareLinkById(sharelinkId, (sharelink) => __awaiter(void 0, void 0, void 0, function* () {
                                if (sharelink != null) {
                                    let UserShareId = sharelink.get("UserShareId", String);
                                    Models_1.MongodbModel.FindUserByUserId(UserShareId, (userShare) => {
                                        if (userShare != null) {
                                            let giftcode = userShare.get("GiftCode", String);
                                            if (giftcode != "") {
                                            }
                                            else {
                                                Models_1.MongodbModel.GetAndDeleteGiftCodeFree((giftCodeFree) => __awaiter(void 0, void 0, void 0, function* () {
                                                    if (giftCodeFree != null) {
                                                        let giftcodevalue = giftCodeFree.get("Value");
                                                        let userShare_id = userShare._id;
                                                        let userShareid = userShare.UserId;
                                                        let userShareName = userShare.get("UserName");
                                                        let giftCodeUsed = Models_1.MongodbModel.GiftCodeUsed(giftcodevalue, userShare_id, userShareid, userShareName, new Date());
                                                        MongodbController_1.MongodbControl.Insert(giftCodeUsed, null);
                                                        let _NameUserShareLink = user.NameUserShareLink;
                                                        userShare.GiftCode = giftcodevalue;
                                                        userShare.KeysFree = 1;
                                                        userShare.NameUserShareLink = _NameUserShareLink;
                                                        yield userShare.save();
                                                    }
                                                }));
                                            }
                                        }
                                    });
                                    let user_id = user._id;
                                    let userid = user.UserId;
                                    let userName = user.UserName;
                                    let UserClick = {
                                        _Id: user_id,
                                        UserId: userid,
                                        UserName: userName
                                    };
                                    sharelink.UsersClickLink.push(UserClick);
                                    yield sharelink.save();
                                }
                            }));
                        }
                    }
                    else { //insert loi
                        let data = {
                            'status': status,
                            'LinkGG': LinkGG,
                            'linkApple': linkApple,
                            'linkVideoLogin': linkVideoLogin,
                            'linkVideoEvent': linkVideoEvent,
                            'UrlItemGiftCode': '',
                            'LinkShareEvent': LinkShareEvent,
                            'LinkFanpageFB': LinkFanpageFB,
                            'Title1': '',
                            'Title2': '',
                            'TermsAndConditions': '',
                            'data': ''
                        };
                        res.send(data);
                        log_1.default.info(Tag + "user == null && insert loi", JSON.stringify(data));
                    }
                }));
            }
        });
    }
    catch (err) {
        let fileName = path.basename(__filename);
        let Tag = fileName + "_" + "post login /";
        log_1.default.error(Tag, err);
    }
}));
router.post('/ShareLink', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let fileName = path.basename(__filename);
    let Tag = fileName + "_" + "post login /ShareLink";
    try {
        let UserShare_id = req.body['UserShare_id'];
        let UserShareId = req.body['UserShareId'];
        console.log('UserShare_id:', UserShare_id);
        console.log('UserShareId:', UserShareId);
        let ShareLink = Models_1.MongodbModel.ShareLink(UserShare_id, UserShareId, [], new Date());
        MongodbController_1.MongodbControl.Insert(ShareLink, (status) => {
            if (status) { //insert thanh cong
                console.log(ShareLink);
                let data = {
                    'status': status,
                    'data': ShareLink._id
                };
                res.send(data);
                log_1.default.info(Tag + " insert thanh cong", JSON.stringify(data));
            }
            else { //insert loi
                let data = {
                    'status': status,
                    'data': ''
                };
                res.send(data);
                log_1.default.info(Tag + " insert loi", JSON.stringify(data));
            }
        });
    }
    catch (err) {
        log_1.default.error(Tag, err);
    }
}));
router.post('/UseKey', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let fileName = path.basename(__filename);
    let Tag = fileName + "_" + "post /UseKey";
    try {
        let id = req.body['id'];
        console.log('id:', id);
        yield Models_1.MongodbModel.FindUserByUserId(id, (doc) => {
            if (doc == null)
                return;
            let KeysFree = doc.KeysFree;
            let KeysUse = doc.KeysUse;
            let GiftCode = doc.GiftCode;
            doc.KeysFree = 0;
            doc.KeysUse += KeysFree;
            doc.save();
            if (KeysFree > 0 && (KeysFree + KeysUse) >= 2) {
                let TimeLimited = '20/04/2020';
                let data = {
                    'status': doc != null,
                    'data': {
                        'KeyFree': KeysFree,
                        'KeyUsed': KeysUse,
                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'GiftCode': GiftCode,
                        'Title1': Title1,
                        'Title2': Title2,
                        'TermsAndConditions': TermsAndConditions,
                    }
                };
                res.send(data);
                log_1.default.info(Tag, JSON.stringify(data));
            }
            else {
                let data = {
                    'status': doc != null,
                    'data': {
                        'KeyFree': KeysFree,
                        'KeyUsed': KeysUse,
                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'GiftCode': '',
                        'Title1': '',
                        'Title2': '',
                        'TermsAndConditions': '',
                    }
                };
                res.send(data);
                log_1.default.info(Tag, JSON.stringify(data));
            }
        });
    }
    catch (err) {
        log_1.default.error(Tag, err);
    }
}));
router.get('/download', function (req, res) {
    //var file = fs.readFileSync(__dirname + '/public/Videos/videotest.mp4', 'binary');
    //var filePath = path.join(__dirname, 'public/Videos');
    var pathdir = process.cwd();
    var filePath = path.join(pathdir, 'public/Videos/videotest.mp4');
    //var fileName = 'videotest.mp4';
    //res.download(filePath);
    //res.attachment(filePath);
    var files = fs.createReadStream(filePath);
    res.writeHead(200, { 'Content-disposition': 'attachment; filename=videotest.mp4' }); //here you can add more headers
    files.pipe(res);
});
exports.default = router;
//# sourceMappingURL=user.js.map