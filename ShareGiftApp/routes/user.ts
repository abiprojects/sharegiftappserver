/*
 * GET users listing.
 */
import express = require('express');
import { MongodbControl }  from '../MongoDataBase/MongodbController';
import { MongodbModel, ShareLink, UserInfo } from '../MongoDataBase/Model/Models';
import logger from '../MyLog/log';
import path = require('path');
import fs = require('fs');
const router = express.Router();
const LinkGG: string = 'https://play.google.com/store/apps/details?id=com.alien.shooter.galaxy.attack';
const linkApple: string = 'https://apps.apple.com/US/app/id1176011642?mt=8';
const linkVideoLogin: string = 'https://sandbox.abigames.com.vn/nodejs/users/download';
const linkVideoEvent: string = 'https://sandbox.abigames.com.vn/nodejs/users/download';
const UrlItemGiftCode: string = 'http://codeskulptor-demos.commondatastorage.googleapis.com/GalaxyInvaders/alien_7_3.png';
const LinkShareEvent: string = 'https://www.facebook.com/GalaxyAttack/';
const LinkFanpageFB: string = 'https://www.facebook.com/GalaxyAttack/';
const Title1: string = 'Ch�c m?ng b?n ?� nh?n ?c gift code';
const Title2: string = 'Event abcxyz s?p ra m?t ..............';
const TermsAndConditions: string = '1. Vui l�ng gi? v� l?u m� ph?n th??ng, ch? c� th? s? d?ng n� 1 l?n tr??c khi h?t h?n.\
2. M� ph?n th??ng s? h?t h?n v�o 1 ng�y ??p tr?i n�o ?�';

enum LoginType {
    FB = 0,
    Line = 1,
    Kakao = 2,
}
function updateShareLink(sharelinkId: string, user: UserInfo): void {
    if (sharelinkId != "") {
        MongodbModel.FindShareLinkById(sharelinkId, async (sharelink) => {
            if (sharelink != null) {
                let UserShareId = sharelink.get("UserShareId", String);
                MongodbModel.FindUserByUserId(UserShareId, (userShare) => {
                    if (userShare != null) {
                        let giftcode = userShare.get("GiftCode", String);
                        if (giftcode != "") {

                        } else {
                            MongodbModel.GetAndDeleteGiftCodeFree((giftCodeFree) => {
                                if (giftCodeFree != null) {
                                    try {
                                        let giftcodevalue = giftCodeFree.Value;
                                        let userShare_id: string = userShare._id;
                                        let userShareid = userShare.UserId;
                                        let userShareName: string = userShare.UserName;
                                        let giftCodeUsed = MongodbModel.GiftCodeUsed(giftcodevalue, userShare_id, userShareid, userShareName, new Date());
                                        MongodbControl.Insert(giftCodeUsed, null);
                                        let _NameUserShareLink: string = user.UserName;
                                        logger.info("", "_NameUserShareLink = " + _NameUserShareLink);
                                        if (userShare.GiftCode == "") userShare.GiftCode = giftcodevalue;
                                        if (userShare.KeysFree < 2) userShare.KeysFree += 1;
                                        if (userShare.NameUserShareLink == "") userShare.NameUserShareLink = _NameUserShareLink;
                                        userShare.save();
                                    } catch (err) {
                                        logger.error("Update usershare ", err);
                                    }
                                }
                            });
                        }
                    }
                });
                let user_id: string = user._id;
                let userid: string = user.UserId;
                let userName: string = user.UserName;
                let UserClick = {
                    _Id: user_id,
                    UserId: userid,
                    UserName: userName
                }
                sharelink.UsersClickLink.push(UserClick);
                await sharelink.save();
            }
        });
    }
}
router.post('/', async (req: express.Request, res: express.Response) => {
    try {
        let fileName: string = path.basename(__filename);
        let Tag: string = fileName + "_" + "post /";
        let name = req.body['name'];
        let id = req.body['id'];
        let sharelinkId = req.body['sharelinkId'];
        let login_type: number = req.body['login_type'];
        let type = LoginType[login_type];
        logger.info(Tag, JSON.stringify(req.body))
        await MongodbModel.FindUserByUserId(id, (user) => {
            if (user != null) {
                if (user.GiftCode != "") {
                    let data = {
                        'status': true,
                        'LinkGG': LinkGG,
                        'linkApple': linkApple,
                        'linkVideoLogin': linkVideoLogin,
                        'linkVideoEvent': linkVideoEvent,
                        'UrlItemGiftCode': UrlItemGiftCode,
                        'LinkShareEvent': LinkShareEvent,
                        'LinkFanpageFB': LinkFanpageFB,
                        'Title1': Title1,
                        'Title2': Title2,
                        'TermsAndConditions': TermsAndConditions ,
                        'data': user
                    };
                    res.send(data);
                    logger.info(Tag + "user != null", JSON.stringify(data))
                } else {
                    let data = {
                        'status': true,
                        'LinkGG': LinkGG,
                        'linkApple': linkApple,
                        'linkVideoLogin': linkVideoLogin,
                        'linkVideoEvent': linkVideoEvent,

                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'LinkFanpageFB': LinkFanpageFB,
                        'Title1': '',
                        'Title2': '',
                        'TermsAndConditions': '',
                        'data': user
                    };
                    res.send(data);
                    logger.info(Tag + "user != null", JSON.stringify(data))
                }
                updateShareLink(sharelinkId, user);
                
            } else {
                let user = MongodbModel.User(name, id, '', 1, 0, login_type, LoginType[type], new Date());
                MongodbControl.Insert<UserInfo>(user, async (status) => {
                    if (status) {//insert thanh cong
                        let data = {
                            'status': status,
                            'LinkGG': LinkGG,
                            'linkApple': linkApple,
                            'linkVideoLogin': linkVideoLogin,
                            'linkVideoEvent': linkVideoEvent,

                            'UrlItemGiftCode': '',
                            'LinkShareEvent': LinkShareEvent,
                            'LinkFanpageFB': LinkFanpageFB,
                            'Title1': '',
                            'Title2': '',
                            'TermsAndConditions': '',
                            'data': user
                        };
                        res.send(data);
                        logger.info(Tag + "user == null && insert thanh cong", JSON.stringify(data))
                        if (sharelinkId != "") {
                            MongodbModel.FindShareLinkById(sharelinkId, async (sharelink) => {
                                if (sharelink != null) {
                                    let UserShareId = sharelink.get("UserShareId", String);
                                    MongodbModel.FindUserByUserId(UserShareId, (userShare) => {
                                        if (userShare != null) {
                                            let giftcode = userShare.get("GiftCode", String);
                                            if (giftcode != "") {

                                            } else {
                                                MongodbModel.GetAndDeleteGiftCodeFree(async (giftCodeFree) => {
                                                    if (giftCodeFree != null) {
                                                        let giftcodevalue = giftCodeFree.get("Value");
                                                        let userShare_id: string = userShare._id;
                                                        let userShareid = userShare.UserId;
                                                        let userShareName: string = userShare.get("UserName");
                                                        let giftCodeUsed = MongodbModel.GiftCodeUsed(giftcodevalue, userShare_id, userShareid, userShareName, new Date());
                                                        MongodbControl.Insert(giftCodeUsed, null);
                                                        let _NameUserShareLink: string = user.NameUserShareLink;
                                                        userShare.GiftCode = giftcodevalue;
                                                        userShare.KeysFree = 1;
                                                        userShare.NameUserShareLink = _NameUserShareLink;
                                                        await userShare.save();
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    let user_id: string = user._id;
                                    let userid: string = user.UserId;
                                    let userName: string = user.UserName;
                                    let UserClick = {
                                        _Id: user_id,
                                        UserId: userid,
                                        UserName: userName
                                    }
                                    sharelink.UsersClickLink.push(UserClick);
                                    await sharelink.save();
                                }
                            });
                        }
                    }
                    else {//insert loi
                        let data = {
                            'status': status,
                            'LinkGG': LinkGG,
                            'linkApple': linkApple,
                            'linkVideoLogin': linkVideoLogin,
                            'linkVideoEvent': linkVideoEvent,

                            'UrlItemGiftCode': '',
                            'LinkShareEvent': LinkShareEvent,
                            'LinkFanpageFB': LinkFanpageFB,
                            'Title1': '',
                            'Title2': '',
                            'TermsAndConditions': '',
                            'data': ''
                        };
                        res.send(data);
                        logger.info(Tag + "user == null && insert loi", JSON.stringify(data))
                    }
                });
            }

        });
    } catch (err) {
        let fileName: string = path.basename(__filename);
        let Tag: string = fileName + "_" + "post login /";
        logger.error(Tag,err);
    }
    
})
router.post('/ShareLink', async (req: express.Request, res: express.Response) => {
    let fileName: string = path.basename(__filename);
    let Tag: string = fileName + "_" + "post login /ShareLink";
    try {
        let UserShare_id = req.body['UserShare_id'];
        let UserShareId = req.body['UserShareId'];
        console.log('UserShare_id:', UserShare_id);
        console.log('UserShareId:', UserShareId);
        let ShareLink = MongodbModel.ShareLink(UserShare_id, UserShareId, [], new Date());
        MongodbControl.Insert(ShareLink, (status) => {
            if (status) {//insert thanh cong
                console.log(ShareLink);
                let data = {
                    'status': status,
                    'data': ShareLink._id
                };
                res.send(data);
                logger.info(Tag + " insert thanh cong", JSON.stringify(data));
            }
            else {//insert loi
                let data = {
                    'status': status,
                    'data': ''
                };
                res.send(data);
                logger.info(Tag + " insert loi", JSON.stringify(data));
            }
        });
    } catch (err) {
        logger.error(Tag, err);
    }
    
})
router.post('/UseKey', async (req: express.Request, res: express.Response) => {
    let fileName: string = path.basename(__filename);
    let Tag: string = fileName + "_" + "post /UseKey";
    try {
        let id = req.body['id'];
        console.log('id:', id);
        await MongodbModel.FindUserByUserId(id, (doc) => {
            if (doc == null) return;
            let KeysFree = doc.KeysFree;
            let KeysUse = doc.KeysUse;
            let GiftCode = doc.GiftCode;
            doc.KeysFree = 0;
            doc.KeysUse += KeysFree;
            doc.save();
            if (KeysFree > 0 && (KeysFree + KeysUse) >= 2) {
                let TimeLimited = '20/04/2020';
                let data = {
                    'status': doc != null,
                    'data': {
                        'KeyFree': KeysFree,
                        'KeyUsed': KeysUse,
                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'GiftCode': GiftCode,
                        'Title1': Title1,
                        'Title2': Title2,
                        'TermsAndConditions': TermsAndConditions,
                    }
                };
                res.send(data);
                logger.info(Tag, JSON.stringify(data))
            } else {
                let data = {
                    'status': doc != null,
                    'data': {
                        'KeyFree': KeysFree,
                        'KeyUsed': KeysUse,
                        'UrlItemGiftCode': '',
                        'LinkShareEvent': LinkShareEvent,
                        'GiftCode': '',
                        'Title1': '',
                        'Title2': '',
                        'TermsAndConditions': '',
                    }
                };
                res.send(data);
                logger.info(Tag, JSON.stringify(data))
            }

        });
    } catch (err) {
        logger.error(Tag, err);
    }
})
router.get('/download', function (req, res) {
    //var file = fs.readFileSync(__dirname + '/public/Videos/videotest.mp4', 'binary');
    //var filePath = path.join(__dirname, 'public/Videos');
    var pathdir = process.cwd();
    var filePath = path.join(pathdir, 'public/Videos/videotest.mp4');
    //var fileName = 'videotest.mp4';
    //res.download(filePath);
    //res.attachment(filePath);
    var files = fs.createReadStream(filePath);
    res.writeHead(200, { 'Content-disposition': 'attachment; filename=videotest.mp4' }); //here you can add more headers
    files.pipe(res)
});
export default router;